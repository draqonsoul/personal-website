import React from "react";
import {Headline} from "draqon-library"
import Router from "../Router/Router";

const App = () => {
    
    return (
        <div className="app">
            <Router />
        </div>
    )
}

export default App;