import React from "react"
import {BrowserRouter, Switch, Route} from "react-router-dom"
import PageIndex from "../../pages/PageIndex/PageIndex";
import PageNotFound from "../../pages/PageNotFound/PageNotFound";
import PageContact from "../../pages/PageContact/PageContact";

interface RouterProps {

}

const Router = ({}: RouterProps) => {
    
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/"> 
                    <PageIndex />
                </Route>
                <Route path="/contact">
                    <PageContact />
                </Route>
                <Route> 
                    <PageNotFound />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Router;