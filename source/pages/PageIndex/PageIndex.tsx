import React from "react"
import {Headline} from "draqon-library"
import "./PageIndex.scss";

interface PageIndexProps {
	className?: string;
}

const PageIndex: React.FC<PageIndexProps> = ({ className }) => {
	
	return (
		<div className={className ? "pageindex "+className : "pageindex"}>
			<Headline size="medium">DraqonDevelops.com</Headline>
            <Headline size="teaser">A Draqons Universe . . .</Headline>
            <Headline size="teaser">Contact Me!</Headline>
		</div>
  );
}

export default PageIndex