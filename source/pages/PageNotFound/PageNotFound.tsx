import React from "react"
import "./PageNotFound.scss";

interface PageNotFoundProps {
	className?: string;
}

const PageNotFound: React.FC<PageNotFoundProps> = ({ className }) => {
	
	return (
		<div className={className ? "pagenotfound "+className : "pagenotfound"}>
			Hello World
		</div>
  );
}

export default PageNotFound