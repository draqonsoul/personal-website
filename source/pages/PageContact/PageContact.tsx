import React from "react"
import "./PageContact.scss";

interface PageContactProps {
	className?: string;
}

const PageContact: React.FC<PageContactProps> = ({ className }) => {
	
	return (
		<div className={className ? "pagecontact "+className : "pagecontact"}>
			Hello World
		</div>
  );
}

export default PageContact