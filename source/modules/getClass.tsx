const getClass = (componentName: string, className?: string, props?: Array<Array<string>>) => {
    let out: string = `${componentName}`
    if(typeof props !== "undefined")
        props.forEach(prop => {
            let propname = prop[0] ?? prop[1] ?? ""
            out += propname !== "" ? ` ${componentName}__${propname}` : ""
        }) 
    if(typeof className !== "undefined") 
        out += ` ${className}`
    return out;
}

export default getClass